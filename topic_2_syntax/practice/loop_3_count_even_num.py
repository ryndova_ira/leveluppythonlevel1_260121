def count_even_num(n):
    """
    Функция count_even_num.

    Принимает натуральное число (целое число > 0).
    Верните количество четных цифр в этом числе.
    Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
    Если число меньше или равно 0, то вернуть "Must be > 0!".
    """

    # if type(n) != int:
    #     return "Must be int!"
    # elif n <= 0:
    #     return "Must be > 0!"
    # else:
    #     count = 0
    #     for i in str(n):
    #         if int(i) % 2 == 0:
    #             count += 1

    if type(n) != int:
        return "Must be int!"

    if n <= 0:
        return "Must be > 0!"

    count = 0
    for i in str(n):
        if int(i) % 2 == 0:
            count += 1

    return count
