# Саша Я
import json


def save_set_to_file_json(path, my_dict):
    """
    Функция save_set_to_file_json.

    Принимает 2 аргумента:
        строка (название файла или полный путь к файлу),
        словарь JSON (для сохранения).

    Сохраняет словарь (JSON) в файл.

    Загрузить словарь JSON (прочитать файл),
    проверить его на корректность.
    """

    with open(path, 'w') as fp:
        json.dump(my_dict, fp, ensure_ascii=False)

    with open(path) as f:
        loaded_dict = json.load(f)

    print(f'my_dict == loaded_dict: {my_dict == loaded_dict}')


if __name__ == '__main__':
    # JSON не умеет хранить ключи как числа (только строки)
    save_set_to_file_json('my_json.json',
                          {1: 'один',
                           2: {3: 'три',
                               4: {5: 'five'}}})

    save_set_to_file_json('my_json.json',
                          {'1': 'один',
                           '2': {'3': 'три',
                                 '4': {'5': 'five'}}})
