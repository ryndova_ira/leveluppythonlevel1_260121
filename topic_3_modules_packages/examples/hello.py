# hello.py

# Код «инициализации» модуля
print('importing hello module')


def hello(name='world'):
    return 'Hello, ' + name


if __name__ == '__main__':
    print("I'm in main function!")
